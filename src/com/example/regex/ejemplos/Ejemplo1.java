package com.example.regex.ejemplos;

import java.util.regex.Pattern;

public class Ejemplo1 {

	public static void main(String[] args) {
		String givenText = "Jorge.";

		Pattern givenPattern = Pattern.compile("(^[A-Z])([a-z]+)\\.$"); // Verifica que la primera letra de una palabra
																		// inicie con Mayúsculas y que al final lleve un
																		// punto

		boolean isRegex = givenPattern.matcher(givenText).matches();
		
		System.out.println("Resultado de la verificación de la expresión: " + isRegex);
	}

}
