package com.example.date.ejemplo2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Ejemplo2 {

	public static void main(String[] args) {
		//Se proporciona el Locale de la fecha que le estamos enviando para evitar conflictos de idioma
		SimpleDateFormat formatter = new SimpleDateFormat("EEEE, MMM dd, yyyy HH:mm:ss a", Locale.ENGLISH);
		String dateInString = "Friday, Jun 7, 2013 12:10:56 PM";

		try {

			Date date = formatter.parse(dateInString);
			System.out.println(date);
			System.out.println(formatter.format(date));

		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

}
