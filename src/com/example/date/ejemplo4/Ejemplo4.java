package com.example.date.ejemplo4;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Ejemplo4 {

	public static void main(String[] args) {
		/*DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
		String date = "16/08/2016";

		// convert String to LocalDate
		LocalDate localDate = LocalDate.parse(date, formatter);
		System.out.println(localDate);*/

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE, MMM d, yyyy hh:mm:ss a", Locale.US);

		String date = "Tuesday, Aug 16, 2016 12:10:56 PM";

		LocalDateTime localDateTime = LocalDateTime.parse(date, formatter);

		System.out.println(localDateTime);

		System.out.println(formatter.format(localDateTime));
	}

}
